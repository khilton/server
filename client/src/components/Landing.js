import React from 'react';


const Landing = () => {
  return(
    <div style={{ textAlign: 'center' }}>
      <h1>
        11Dev
      </h1>
      Manage your club
    </div>
  )
}

export default Landing;
